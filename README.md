## Visit website 
 [App Demo](https://juniordevtestb.herokuapp.com/)

## Tech Stack

- HTML
- SASS
- PHP
- MySQL
- jQuery


## Get started on your local machine

**1.**  Clone a copy of the project with git clone https://Flowry@bitbucket.org/Flowry/item-pages.git

**2.**  Navigate to the project directory


## Get your server running locally

**1.**  Download XAMPP here (https://www.apachefriends.org/download.html)

**2.**  Follow instructions to install

**3.**  Open XAMPP Control Panel

**4.**  Navigate to Manage Servers tab in the XAMPP menu

**5.**  Click on Start All button

**6.**  Servers should be running if there are no issues

**7.**  To view app, navigate to Welcome in the XAMPP menu, click on Go to Application and add the app folder        directory